<?php

namespace AppBundle\Service;

use AppBundle\Service\Repository\ImageRepository;
use Knp\Component\Pager\Paginator;

/**
 * Class ImageService
 * @package AppBundle\Service
 */
class ImageService
{
    /**
     * @var ImageRepository
     */
    protected $imageRepository;

    /**
     * @var Paginator
     */
    protected $knpPaginator;

    /**
     * @var int
     */
    protected $limitPerPage;

    /**
     * AlbumService constructor.
     * @param ImageRepository $imageRepository
     * @param Paginator $knpPaginator
     * @param int $limitPerPage
     */
    public function __construct(ImageRepository $imageRepository, Paginator $knpPaginator, $limitPerPage)
    {
        $this->imageRepository = $imageRepository;
        $this->knpPaginator    = $knpPaginator;
        $this->limitPerPage    = $limitPerPage;
    }

    /**
     * @param int $albumId Id of album
     * @param int $page Id of page
     * @return \Knp\Component\Pager\Pagination\PaginationInterface
     */
    public function fetchImagesWithPagination($albumId, $page)
    {
        $imageQuery = $this->imageRepository->fetchForAlbum($albumId);
        $pagination = $this->knpPaginator->paginate(
            $imageQuery,
            $page,
            $this->limitPerPage
        );
        return $pagination;
    }
}