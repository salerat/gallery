<?php

namespace AppBundle\Service\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class AlbumRepository
 * @package AppBundle\Service\Repository
 */
class AlbumRepository extends EntityRepository
{

    /**
     * @return array
     */
    public function fetchAll()
    {
        return $this->createQueryBuilder("p")
            ->getQuery()
            ->getResult();
    }
}