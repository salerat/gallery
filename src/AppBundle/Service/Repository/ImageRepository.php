<?php

namespace AppBundle\Service\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class ImageRepository
 * @package AppBundle\Service\Repository
 */
class ImageRepository extends EntityRepository
{
    /**
     * @param int $albumId Id of album
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function fetchForAlbum($albumId)
    {
        return $this->createQueryBuilder("p")
            ->where("p.album = :albumId")
            ->setParameter('albumId', $albumId);
    }
}