<?php

namespace AppBundle\Service;

use AppBundle\Service\Repository\AlbumRepository;
use Knp\Component\Pager\Paginator;

/**
 * Class AlbumService
 * @package AppBundle\Service
 */
class AlbumService
{
    /**
     * @var AlbumRepository
     */
    protected $albumRepository;

    /**
     * AlbumService constructor.
     * @param AlbumRepository $albumRepository
     */
    public function __construct(AlbumRepository $albumRepository)
    {
        $this->albumRepository = $albumRepository;
    }

    /**
     * @return \Knp\Component\Pager\Pagination\PaginationInterface
     */
    public function fetchAlbums()
    {
        return $this->albumRepository->fetchAll();
    }
}