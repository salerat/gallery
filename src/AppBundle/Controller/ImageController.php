<?php

namespace AppBundle\Controller;

use AppBundle\Service\ImageService;
use JMS\Serializer\Serializer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ImageController
 * @package AppBundle\Controller
 */
class ImageController extends Controller
{
    /**
     * @param int $albumId Album key
     * @param int $page Page Number
     * @return Response
     */
    public function listAction($albumId, $page)
    {
        /**
         * @var Serializer $serializer
         */
        $serializer = $this->get('jms_serializer');
        /**
         * @var ImageService $albumService
         */
        $imageService = $this->get('image');
        $albums = $imageService->fetchImagesWithPagination($albumId, $page);
        return new Response($serializer->serialize($albums, 'json'));
    }
}
