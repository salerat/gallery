<?php

namespace AppBundle\Controller;

use AppBundle\Service\AlbumService;
use JMS\Serializer\Serializer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AlbumController
 * @package AppBundle\Controller
 */
class AlbumController extends Controller
{
    /**
     * @return Response
     */
    public function listAction()
    {
        /**
         * @var Serializer $serializer
         */
        $serializer = $this->get('jms_serializer');
        /**
         * @var AlbumService $albumService
         */
        $albumService = $this->get('album');
        $albums = $albumService->fetchAlbums();
        return new Response($serializer->serialize($albums, 'json'));
    }
}
