<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AlbumControllerTest extends WebTestCase
{
    public function listDataProvider()
    {
        return array(
            array(
                //here is should be paginator instance
                array('some data'),
            )
        );
    }
    /**
     * @dataProvider listDataProvider
     */
    public function testList($albums)
    {
        $client = static::createClient();

        $serviceAlbum = $this->getMockBuilder('AlbumService')
            ->disableOriginalConstructor()
            ->setMethods(array('fetchAlbums'))
            ->getMock();
        $serviceAlbum->expects($this->once())
            ->method('fetchAlbums')
            ->will($this->returnValue($albums));

        $client->getContainer()->set('album', $serviceAlbum);

        $serializer = $client->getContainer()->get('jms_serializer');

        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals($albums, $serializer->deserialize($client->getResponse()->getContent(), 'array', 'json'));
    }
}
