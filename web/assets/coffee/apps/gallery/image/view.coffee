define ["js/app", "tpl!js/apps/gallery/image/templates/image_item.tpl", "tpl!js/apps/gallery/image/templates/image_title.tpl", "tpl!js/apps/gallery/image/templates/image_layout.tpl", "tpl!js/apps/gallery/image/templates/pagination_item.tpl"], (App, imageItemTpl, imageTitleTpl, imageLayoutTpl, paginatorTpl) ->
  App.module "Gallery.View", (View, App, Backbone, Marionette, $, _) ->

    View.ImageView = Marionette.ItemView.extend(
      tagName: "tr",
      template: imageItemTpl
    )

    View.ImageTitleView = Marionette.ItemView.extend(
      template: imageTitleTpl
    )

    View.ImageCollectionView = Marionette.CollectionView.extend(
      tagName: "table",
      className: "table",
      childView: View.ImageView
    )

    View.PaginatorView = Marionette.ItemView.extend(
      tagName: "div",
      className: "pagination-box",
      template: paginatorTpl
    )

    View.PaginatorCollectionView = Marionette.CollectionView.extend(
      childView: View.PaginatorView
    )

    View.ImageLayoutView = Marionette.LayoutView.extend(
      template: imageLayoutTpl,
      regions: {
        tableTitleRegion: "#table-title-region",
        tableRegion: "#table-region",
        paginatorRegion: "#paginator-region"
      }
    )

  App.Gallery.View