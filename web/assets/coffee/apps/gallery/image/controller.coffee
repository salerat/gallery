define ["js/app", "js/apps/gallery/image/view"], (App, View) ->
  App.module "Gallery.Image", (Image, App, Backbone, Marionette, $, _) ->
    Image.Controller =
      ImageModel =
      displayImages: (id, page) ->
        callback = (response) ->
          imageCollection = new View.ImageCollectionView({collection: new Backbone.Collection(response.items)})
          layout = new View.ImageLayoutView()
          App.mainRegion.show layout
          layout.getRegion('tableTitleRegion').show new View.ImageTitleView()
          layout.getRegion('tableRegion').show imageCollection
          if response.total_count > response.num_items_per_page
            require ["js/entities/paginator"], ->
              getPaginator = App.request "paginator", id, response.total_count, response.num_items_per_page
              $.when(getPaginator).done (paginator) ->
                layout.getRegion('paginatorRegion').show new View.PaginatorCollectionView({collection: paginator})
        route = '/album/' + id
        if page
          route += '/page/' + page
        $.get route, {}, callback, 'json'

  App.Gallery.Image.Controller