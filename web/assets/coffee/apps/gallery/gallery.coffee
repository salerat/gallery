define ["js/app"], (App) ->
  App.module "Gallery", (Gallery, App, Backbone, Marionette, $, _) ->
    App.Router = Marionette.AppRouter.extend(
      appRoutes:
        main: "displayAlbum",
        "album/:id": "displayImages",
        "album/:id/page/:page": "displayImages"
    )
    API =
      displayAlbum: ->
        require ["js/apps/gallery/album/controller"], ->
          App.Gallery.Album.Controller.displayAlbum()
      displayImages: (id, page) ->
        require ["js/apps/gallery/image/controller"], ->
          App.Gallery.Image.Controller.displayImages(id, page)

    App.on "landing:home", ->
      API.displayAlbum()

    App.addInitializer ->
      new App.Router(controller: API)

  App.Gallery