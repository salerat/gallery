define ["js/app", "js/apps/gallery/album/view"], (App, View) ->
  App.module "Gallery.Album", (Album, App, Backbone, Marionette, $, _) ->
    Album.Controller =
      AlbumModel =
      displayAlbum: ->
        callback = (response) ->
          albumCollection = new View.AlbumCollectionView({collection: new Backbone.Collection(response)})
          layout = new View.AlbumLayoutView()
          App.mainRegion.show layout
          layout.getRegion('tableTitleRegion').show new View.AlbumTitleView()
          layout.getRegion('tableRegion').show albumCollection
        $.get '/', {}, callback, 'json'

  App.Gallery.Album.Controller