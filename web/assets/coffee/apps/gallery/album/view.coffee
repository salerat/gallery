define ["js/app", "tpl!js/apps/gallery/album/templates/album_item.tpl", "tpl!js/apps/gallery/album/templates/album_title.tpl", "tpl!js/apps/gallery/album/templates/album_layout.tpl"], (App, albumItemTpl, albumTitleTpl, albumLayoutTpl) ->
  App.module "Gallery.View", (View, App, Backbone, Marionette, $, _) ->

    View.AlbumView = Marionette.ItemView.extend(
      tagName: "tr",
      template: albumItemTpl
    )

    View.AlbumTitleView = Marionette.ItemView.extend(
      template: albumTitleTpl
    )

    View.AlbumCollectionView = Marionette.CollectionView.extend(
      tagName: "table",
      className: "table",
      childView: View.AlbumView
    )

    View.AlbumLayoutView = Marionette.LayoutView.extend(
      template: albumLayoutTpl,
      regions: {
        tableTitleRegion: "#table-title-region",
        tableRegion: "#table-region"
      }
    )

  App.Gallery.View