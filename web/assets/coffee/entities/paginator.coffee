define ["js/app"], (App) ->
  App.module "Entities", (Entities, App, Backbone, Marionette, $, _) ->
    Entities.Paginator = Backbone.Collection.extend()

    API =
      getPaginatorEntity: (id, lastId, perPage) ->
        defer = $.Deferred()
        defer.resolve new Entities.Paginator(API.getPaginatorModel(id, lastId, perPage))
        defer.promise()

      getPaginatorModel: (id, lastId, perPage) ->
        pagesNumber = Math.ceil lastId/perPage
        resultArray = [];
        i = 1

        col = new Entities.Paginator()

        while i < pagesNumber + 1
          resultArray.push {album: parseInt(id), page: i}
          i++
        resultArray

    App.reqres.setHandler "paginator", (id, lastId, perPage) ->
      API.getPaginatorEntity(id, lastId, perPage)

  return