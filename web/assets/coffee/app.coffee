define ["marionette"], (Marionette) ->

  App = new Marionette.Application()

  App.addRegions
    mainRegion: "#main-region"
    dialogRegion: "#dialog-Region"

  # route helpers
  App.navigate = (route, options) ->
    options or (options = {})
    Backbone.history.navigate route, options

  App.getCurrentRoute = ->
    Backbone.history.fragment

  App.on "start", ->
    require ["js/apps/gallery/gallery"], ->
      console.log "Marionette Application Started"
      if Backbone.history
        if !Backbone.History.started
          Backbone.history.start()
        if App.getCurrentRoute() is ""
          App.navigate "main"
          App.trigger "landing:home"

  App