module.exports = function (grunt) {
    'use strict';
    var config = {
        app: 'app',
        dist: 'dist'
    };
    grunt.initConfig({
        config: config,
        watch: {
            coffee: {
                files: ['web/assets/coffee/{,**/}*.coffee'],
                tasks: ['coffee:glob_to_multiple','coffee:amdConfig', 'shell:mocha-phantomjs']
            },
            templates: {
                files: ['web}/assets/**/templates/*.tpl'],
                tasks: ['copy:templates', 'coffe:glob_to_multiple']
            },
            indextemplate: {
                files: ['web/template.html'],
                tasks: ['targethtml:app']
            },
            css: {
                files: ['./web/assets/bower_components/bootstrap/dist/css/bootstrap.css', './web/assets/css/app.css'],
                 tasks: ['cssmin:combine'] 
            }
        },
        // coffeescript
        coffee: {
            glob_to_multiple: {
                expand: true,
                flatten: false,
                bare: true,
                cwd: 'web/assets/coffee/',
                src: ['**/*.coffee', '*.coffee'],
                dest: 'web/assets/js/',
                ext: '.js'
            },
            amdConfig: {
                flatten: true,
                options: {
                    bare: true
                },
                cwd: './',
                src: ['web/assets/require_main.coffee'],
                dest: 'web/assets/require_main.js'
            }
        },
        //copy templates
        copy: {
            templates: {
                files: [{
                    expand: true,
                    cwd: 'web/assets/coffee/',
                    src: ['**/*.{tpl,md}'],
                    dest: 'web/assets/js/'
                }]
            },
            requireBuilt: {
                files: [{
                    cwd: './',
                    src: ['web/assets/require_main_built.js'],
                    dest: '<%= config.dist %>/js/require_main_built.js'
                }]
            }
        },
        shell: {
            'buildRequire': {
                command: 'node r.js -o assets/build.js',
                options: {
                    stdout: true,
                    execOptions: {
                        cwd: './app'
                    }
                }
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-coffee');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-shell');
    grunt.registerTask('default', ['dev', 'watch']);
    grunt.task.registerTask('dev', 'subset of common development tasks used in other tasks', function () {
        grunt.task.run(['copy:templates', 'coffee'
        ]);
    });
    grunt.task.registerTask('build', 'creates optimized distribution', function () {
        grunt.task.run(['dev', 'shell:buildRequire', 'copy:requireBuilt', 'cssmin:minify', 'express:dist', 'open:build', 'watch:indextemplate']);
    });
};