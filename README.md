### 1) composer install
### 2) npm install
### 3) in web folder bower install
### 4) to compile coffe run grunt in root folder
### 5) upload db gallery.sql
### 6) use it from http://gallery.dev/index.html
### 7) today i can't upload this project to server but i recorded a video https://www.dropbox.com/s/d4wm9h720r0oyez/Screencast%202016-04-11%2004%3A01%3A39.mp4?dl=0
#### for example my nginx config

> server {
>	root /var/www/symfony/gallery/web;
>	index index.html;
>
>	listen 127.0.0.6:80 default_server;
>	server_name gallery.dev;
>	access_log /var/log/nginx/galley_access.log;
>	error_log /var/log/nginx/galley_error.log;
>
>	location ~ ^/(scripts.*js|styles|images) {
>		gzip_static on;
>		expires 1y;
>		add_header Cache-Control public;
>		add_header ETag "";
>
>		break;
>	}
>	location / {
>		try_files $uri $uri/ /app_dev.php$is_args$args $uri/index.html index.html;
>	}
>
>	location @rewrite {
>		rewrite ^/(.*)$ /app_dev.php?q=$1;
>	}
>
>	location ~ ^/(app_dev|config)\.php(/|$) {
>               fastcgi_split_path_info ^(.+\.php)(/.*)$;
>		fastcgi_pass unix:/var/run/php5-fpm.sock;
>		fastcgi_index index.php;
>		include fastcgi_params;
>		fastcgi_param  SCRIPT_FILENAME /var/www/symfony/gallery/web/$fastcgi_script_name;
>		fastcgi_param  AGATE_ENVIRONMENT dev;
>	}
>
>	location ~ /\.git {
>		deny all;
>	}
> }