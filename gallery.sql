-- phpMyAdmin SQL Dump
-- version 4.5.5.1deb2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 11, 2016 at 04:15 AM
-- Server version: 5.5.44-0+deb8u1
-- PHP Version: 5.6.19-2+b1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gallery`
--

-- --------------------------------------------------------

--
-- Table structure for table `album`
--

CREATE TABLE `album` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `name_key` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `album`
--

INSERT INTO `album` (`id`, `name`, `name_key`) VALUES
(1, 'Test Album 1', 'testalbum1'),
(2, 'Test Album 2', 'testalbum2'),
(3, 'Test Album 3', 'testalbum3'),
(4, 'Test Album 4', 'testalbum4'),
(5, 'Test Album 5', 'testalbum5');

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE `image` (
  `id` int(11) NOT NULL,
  `album_id` int(11) NOT NULL,
  `url` varchar(128) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `image`
--

INSERT INTO `image` (`id`, `album_id`, `url`, `name`) VALUES
(1, 1, '/assets/album_img/testalbum1/plane.jpg', 'Image 1'),
(2, 1, '/assets/album_img/testalbum1/plane.jpg', 'Image 2'),
(3, 1, '/assets/album_img/testalbum1/plane.jpg', 'Image 3'),
(4, 1, '/assets/album_img/testalbum1/plane.jpg', 'Image 4'),
(5, 1, '/assets/album_img/testalbum1/plane.jpg', 'Image 5'),
(6, 2, '/assets/album_img/testalbum2/car.jpg', 'Image 1'),
(7, 2, '/assets/album_img/testalbum2/car.jpg', 'Image 2'),
(8, 2, '/assets/album_img/testalbum2/car.jpg', 'Image 3'),
(9, 2, '/assets/album_img/testalbum2/car.jpg', 'Image 4'),
(10, 2, '/assets/album_img/testalbum2/car.jpg', 'Image 5'),
(11, 2, '/assets/album_img/testalbum2/car.jpg', 'Image 6'),
(12, 2, '/assets/album_img/testalbum2/car.jpg', 'Image 7'),
(13, 2, '/assets/album_img/testalbum2/car.jpg', 'Image 8'),
(14, 2, '/assets/album_img/testalbum2/car.jpg', 'Image 9'),
(15, 2, '/assets/album_img/testalbum2/car.jpg', 'Image 10'),
(16, 2, '/assets/album_img/testalbum2/car.jpg', 'Image 11'),
(17, 2, '/assets/album_img/testalbum2/car.jpg', 'Image 12'),
(18, 2, '/assets/album_img/testalbum2/car.jpg', 'Image 13'),
(19, 2, '/assets/album_img/testalbum2/car.jpg', 'Image 14'),
(20, 2, '/assets/album_img/testalbum2/car.jpg', 'Image 15'),
(21, 2, '/assets/album_img/testalbum2/car.jpg', 'Image 16'),
(22, 2, '/assets/album_img/testalbum2/car.jpg', 'Image 17'),
(23, 2, '/assets/album_img/testalbum2/car.jpg', 'Image 18'),
(24, 2, '/assets/album_img/testalbum2/car.jpg', 'Image 19'),
(25, 2, '/assets/album_img/testalbum2/car.jpg', 'Image 20'),
(26, 2, '/assets/album_img/testalbum2/car.jpg', 'Image 21'),
(27, 2, '/assets/album_img/testalbum2/car.jpg', 'Image 22'),
(28, 3, '/assets/album_img/testalbum3/debian.png', 'Image 1'),
(29, 3, '/assets/album_img/testalbum3/debian.png', 'Image 2'),
(30, 3, '/assets/album_img/testalbum3/debian.png', 'Image 3'),
(31, 3, '/assets/album_img/testalbum3/debian.png', 'Image 4'),
(32, 3, '/assets/album_img/testalbum3/debian.png', 'Image 5'),
(33, 3, '/assets/album_img/testalbum3/debian.png', 'Image 6'),
(34, 3, '/assets/album_img/testalbum3/debian.png', 'Image 7'),
(35, 3, '/assets/album_img/testalbum3/debian.png', 'Image 8'),
(36, 3, '/assets/album_img/testalbum3/debian.png', 'Image 9'),
(37, 3, '/assets/album_img/testalbum3/debian.png', 'Image 10'),
(38, 3, '/assets/album_img/testalbum3/debian.png', 'Image 11'),
(39, 3, '/assets/album_img/testalbum3/debian.png', 'Image 12'),
(40, 3, '/assets/album_img/testalbum3/debian.png', 'Image 13'),
(41, 3, '/assets/album_img/testalbum3/debian.png', 'Image 14'),
(42, 3, '/assets/album_img/testalbum3/debian.png', 'Image 15'),
(43, 3, '/assets/album_img/testalbum3/debian.png', 'Image 16'),
(44, 3, '/assets/album_img/testalbum3/debian.png', 'Image 17'),
(45, 3, '/assets/album_img/testalbum3/debian.png', 'Image 18'),
(46, 3, '/assets/album_img/testalbum3/debian.png', 'Image 19'),
(47, 3, '/assets/album_img/testalbum3/debian.png', 'Image 20'),
(48, 3, '/assets/album_img/testalbum3/debian.png', 'Image 21'),
(49, 3, '/assets/album_img/testalbum3/debian.png', 'Image 22'),
(50, 4, '/assets/album_img/testalbum4/pc.png', 'Image 1'),
(51, 4, '/assets/album_img/testalbum4/pc.png', 'Image 2'),
(52, 4, '/assets/album_img/testalbum4/pc.png', 'Image 3'),
(53, 4, '/assets/album_img/testalbum4/pc.png', 'Image 4'),
(54, 4, '/assets/album_img/testalbum4/pc.png', 'Image 5'),
(55, 4, '/assets/album_img/testalbum4/pc.png', 'Image 6'),
(56, 4, '/assets/album_img/testalbum4/pc.png', 'Image 7'),
(57, 4, '/assets/album_img/testalbum4/pc.png', 'Image 8'),
(58, 4, '/assets/album_img/testalbum4/pc.png', 'Image 9'),
(59, 4, '/assets/album_img/testalbum4/pc.png', 'Image 10'),
(60, 4, '/assets/album_img/testalbum4/pc.png', 'Image 11'),
(61, 4, '/assets/album_img/testalbum4/pc.png', 'Image 12'),
(62, 4, '/assets/album_img/testalbum4/pc.png', 'Image 13'),
(63, 4, '/assets/album_img/testalbum4/pc.png', 'Image 14'),
(64, 4, '/assets/album_img/testalbum4/pc.png', 'Image 15'),
(65, 4, '/assets/album_img/testalbum4/pc.png', 'Image 16'),
(66, 4, '/assets/album_img/testalbum4/pc.png', 'Image 17'),
(67, 4, '/assets/album_img/testalbum4/pc.png', 'Image 18'),
(68, 4, '/assets/album_img/testalbum4/pc.png', 'Image 19'),
(69, 4, '/assets/album_img/testalbum4/pc.png', 'Image 20'),
(70, 4, '/assets/album_img/testalbum4/pc.png', 'Image 21'),
(71, 4, '/assets/album_img/testalbum4/pc.png', 'Image 22'),
(72, 5, '/assets/album_img/testalbum5/mouse.jpg', 'Image 1'),
(73, 5, '/assets/album_img/testalbum5/mouse.jpg', 'Image 2'),
(74, 5, '/assets/album_img/testalbum5/mouse.jpg', 'Image 3'),
(75, 5, '/assets/album_img/testalbum5/mouse.jpg', 'Image 4'),
(76, 5, '/assets/album_img/testalbum5/mouse.jpg', 'Image 5'),
(77, 5, '/assets/album_img/testalbum5/mouse.jpg', 'Image 6'),
(78, 5, '/assets/album_img/testalbum5/mouse.jpg', 'Image 7'),
(79, 5, '/assets/album_img/testalbum5/mouse.jpg', 'Image 8'),
(80, 5, '/assets/album_img/testalbum5/mouse.jpg', 'Image 9'),
(81, 5, '/assets/album_img/testalbum5/mouse.jpg', 'Image 10'),
(82, 5, '/assets/album_img/testalbum5/mouse.jpg', 'Image 11'),
(83, 5, '/assets/album_img/testalbum5/mouse.jpg', 'Image 12'),
(84, 5, '/assets/album_img/testalbum5/mouse.jpg', 'Image 13'),
(85, 5, '/assets/album_img/testalbum5/mouse.jpg', 'Image 14'),
(86, 5, '/assets/album_img/testalbum5/mouse.jpg', 'Image 15'),
(87, 5, '/assets/album_img/testalbum5/mouse.jpg', 'Image 16'),
(88, 5, '/assets/album_img/testalbum5/mouse.jpg', 'Image 17'),
(89, 5, '/assets/album_img/testalbum5/mouse.jpg', 'Image 18'),
(90, 5, '/assets/album_img/testalbum5/mouse.jpg', 'Image 19'),
(91, 5, '/assets/album_img/testalbum5/mouse.jpg', 'Image 20'),
(92, 5, '/assets/album_img/testalbum5/mouse.jpg', 'Image 21'),
(93, 5, '/assets/album_img/testalbum5/mouse.jpg', 'Image 22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_key` (`name_key`);

--
-- Indexes for table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `album_id` (`album_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `album`
--
ALTER TABLE `album`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `image`
--
ALTER TABLE `image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `image`
--
ALTER TABLE `image`
  ADD CONSTRAINT `image_ibfk_1` FOREIGN KEY (`album_id`) REFERENCES `album` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
